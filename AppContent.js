import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from "react-native";
import MainNavigator from './src/navigation/MainNavigator';
import { errorSelector } from './src/store/app/app.selectors';
import { resetError } from './src/store/app/app.actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ErrorToast from './src/components/ErrorToast';

const AppContent = ({ error, resetError }) => {
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content" />
      <MainNavigator />
      <ErrorToast visible={!!error} message={error} onClose={() => resetError()} />
    </NavigationContainer>
  )
}

const mapStateToProps = state => ({
  error: errorSelector(state)
})

const mapDispatchToProps = dispatch => bindActionCreators({
  resetError,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppContent);