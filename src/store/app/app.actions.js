import { APP_ACTION_TYPES } from './app.types';

export const setError = msg => ({
  type: APP_ACTION_TYPES.SET_ERROR_MSG,
  payload: (msg && typeof msg === "string") ? msg : (!msg ? null : "Error en el sistema"),
});

export const resetError = () => ({
  type: APP_ACTION_TYPES.SET_ERROR_MSG,
  payload: null,
})