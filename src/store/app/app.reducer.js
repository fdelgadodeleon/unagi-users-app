import { APP_ACTION_TYPES } from "./app.types";

export const INITIAL_STATE = {
  errorMsg: null,
};

const appReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case APP_ACTION_TYPES.SET_ERROR_MSG:
      return {
        ...state,
        errorMsg: action.payload
      };
    default:
      return state;
  }
};

export default appReducer;