import React from 'react';
import { Image, StyleSheet } from 'react-native';

const Avatar = ({ uri, size }) => {

  const styles = StyleSheet.create({
    avatar: {
      width: size || 50,
      height: size || 50,
      borderRadius: size || 50
    }
  });
  return (
    <Image
      style={styles.avatar}
      source={{
        uri: uri,
      }}
    />
  )
}


export default Avatar;