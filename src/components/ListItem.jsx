import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const ListItem = ({ left, right, title, subtitle }) => {
  return (
    <View style={styles.container}>
      {left &&
        <View style={styles.left}>
          {left}
        </View>
      }
      <View style={styles.center}>
        <Text style={styles.title}>{title}</Text>
        {subtitle && <Text style={styles.subtitle}>{subtitle}</Text>}
      </View>
      {right &&
        <View style={styles.right}>
          {right}
        </View>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: '#fff',
    alignItems: 'center',
    borderBottomColor: 'lightgray',
    borderBottomWidth: .5,
    padding: 8
  },
  left: {
    flex: 1
  },
  right: {
    flex: 1
  },
  center: {
    flex: 4,
    paddingLeft: 8,
    paddingRight: 8
  },
  title: {
    fontSize: 16
  },
  subtitle: {
    color: "lightgray",
    fontSize: 12
  }
});

export default ListItem;
