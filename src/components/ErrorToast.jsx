import React from 'react';
import { Modal, StyleSheet, View, Button, Text } from "react-native";

const ErrorToast = ({ visible, onClose, message }) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={visible}
      onRequestClose={onClose}
    >
      <View style={styles.bottomView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>{message}</Text>
          <Button color="white" onPress={onClose} title="OK" />
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  bottomView: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "darkred",
    borderRadius: 5,
    padding: 10,
    width: "90%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalText: {
    marginRight: 10,
    textAlign: "center",
    color: "white",
    fontSize: 16,
  }
});

export default ErrorToast;