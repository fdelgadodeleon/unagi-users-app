import { errorHandler } from "./errorHandler";

export const runFetch = (url, params = {}) => {
  return fetch(url, params)
    .then(response => {
      if (response.ok) {
        return response.status !== 204
          ? Promise.resolve(response.json())
          : Promise.resolve();
      } else {
        return Promise.reject(response)
      }
    })
    .catch(error => Promise.reject(errorHandler(error)))
};
