export const errorHandler = error => {
  if (error.status) {
    switch (error.status) {
      case 500:
        return "Hubo un error en el servidor"
        break;
      case 400:
        return "Hubo un error en la consulta"
        break;
      case 401:
        return "El usuario no está autorizado"
        break;
      case 403:
        return "El usuario no tiene permisos"
        break;
      default:
        return "Hubo un error en la consulta"
    }
  } else {
    return "Hubo un error de red"
  }
};
