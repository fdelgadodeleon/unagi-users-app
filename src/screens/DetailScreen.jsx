import React, { useEffect, useState } from 'react';
import { Text, View, ActivityIndicator, StyleSheet, SafeAreaView } from 'react-native';
import Avatar from '../components/Avatar';
import { requests } from '../utils/requestsHandler';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setError } from '../store/app/app.actions';

const DetailScreen = ({ route, setError }) => {
  const { id } = route.params
  const [loading, setLoading] = useState(false)
  const [user, setUser] = useState(null)

  /**
   * Fetch user data when component is mounted
   */
  useEffect(() => {
    setLoading(true)
    requests.get(`https://reqres.in/api/users/${id}`)
      .then(res => setUser(res.data))
      .catch(err => setError(err))
      .then(() => setLoading(false))
  }, [id])

  return (
    <SafeAreaView style={styles.container}>
      {loading && <ActivityIndicator size="large" />}
      {user && (
        <View style={styles.userContainer}>
          <View style={styles.avatar}>
            <Avatar uri={user.avatar} size={200} />
          </View>
          <Text style={styles.title}>{`${user.first_name} ${user.last_name}`}</Text>
          <Text style={styles.subtitle}>{user.email}</Text>
        </View>
      )}
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    margin: 20,
    flex: 1,
  },
  userContainer: {
    backgroundColor: 'white',
    borderRadius: 10,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    marginBottom: 50
  },
  title: {
    fontSize: 28,
  },
  subtitle: {
    fontSize: 16,
  }
});
const mapDispatchToProps = dispatch => bindActionCreators({
  setError,
}, dispatch);

export default connect(
  null,
  mapDispatchToProps,
)(DetailScreen);