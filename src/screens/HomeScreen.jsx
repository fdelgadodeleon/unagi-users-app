import React, { useEffect, useState } from 'react';
import { SafeAreaView, TouchableOpacity, StyleSheet, ActivityIndicator, FlatList } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import ListItem from '../components/ListItem';
import Avatar from '../components/Avatar';
import { requests } from '../utils/requestsHandler';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setError } from '../store/app/app.actions';

const HomeScreen = ({ navigation, setError }) => {
  const [loading, setLoading] = useState(false)
  const [users, setUsers] = useState(null)

  /**
   * Fetch users data when component is mounted
   */
  useEffect(() => {
    fetchUsers()
  }, [])

  const fetchUsers = () => {
    setLoading(true)
    requests.get('https://reqres.in/api/users')
      .then(res => setUsers(res.data))
      .catch(err => setError(err))
      .then(() => setLoading(false))
  }

  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => navigation.navigate('Detail', { id: item.id })} >
      <ListItem
        title={`${item.first_name} ${item.last_name}`}
        left={<Avatar uri={item.avatar} />}
        right={<MaterialIcons name="chevron-right" size={36} color="gray" />}
      />
    </TouchableOpacity>
  )

  return (
    <SafeAreaView style={styles.container}>
      {users && users.length > 0 && (
        <FlatList
          onRefresh={fetchUsers}
          refreshing={loading}
          data={users}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
        />
      )}
    </SafeAreaView >
  )
}

const styles = StyleSheet.create({
  container: {
    margin: 20,
    flex: 1
  }
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setError,
}, dispatch);

export default connect(
  null,
  mapDispatchToProps,
)(HomeScreen);